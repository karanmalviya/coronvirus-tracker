# Covid-19 Tracker

Covid-19 tracker is basically gives us information about coronvirus cases, deaths and recovered around india and users can modify their searching and can search for their perticular preferences, i,e. Their own state and district.
We have also added a simulation how it looks when we go on the page of vaccine registration. We have created simulation so user can get the idea of how the vaccine registration process works and they can if their registration is successful or not.


Major contributors and members of this project
Karan Malviya (https://github.com/Karanmalviya) ,

# Coronavirus Tracker Application

Covid-19 tracker app is basically gives in information about coronvirus cases, deaths and recovered and we can search in it country wise pandemic  

![209a2a9f615ca0f75e20b8feae7095da3e268a76](https://user-images.githubusercontent.com/72023877/165787593-b481847e-09c5-45e3-944e-7a0348c69450.gif)

# Running setup

```bash 
    pip install requests
    
    pip install bs4
```

Steps to use:

1.Firstly Connect your pc to the Stable Internet Connection.

2.Then run setup.py file for running application. 

3.Then after opening the application there are 4 options.

4.first option country in which you can see the coronvirus cases information of mentioned countries (realtime cases).

5.second option state in which you can see the coronvirus cases information of mentioned indian states (realtime cases). 

6.third option district in which you can see the coronvirus cases information of mentioned indian states selected districts (realtime cases).

7.fourth option vaccination in which you can view slot or book slot of vaccination (demo booking).
